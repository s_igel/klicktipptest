######################################
# Klick Tipp test task via Fairmondo #
# Author: Susan Igel                 #
# Date: 05.12.2019                   #
######################################

from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
import pytest


# Fixture for pre and post conditions:
@pytest.fixture()
def test_setup():
    global driver
    driver = webdriver.Firefox(executable_path="C:/Users/susan/PycharmProjects/Testing/Drivers/geckodriver.exe")
    driver.implicitly_wait(10)
    driver.maximize_window()
    yield
    driver.close()
    driver.quit()

# Actual start of test execution:
def test_fairmondo(test_setup):
    # Open Fairmondo homepage and check title of page:
    driver.get("https://www.fairmondo.de/")
    assert driver.title == "Fairmondo"
    # Click on "Warenkorb" in the menu area:
    driver.find_element_by_link_text('Warenkorb (0)').click()
    assert ('Dein Warenkorb' in driver.page_source)
    # Check if basket is empty
    empty_cart = "Dein Warenkorb ist leer." in driver.page_source
    if empty_cart == True:
        # Locate search field and search for 'Kaffee':
        driver.find_element_by_name('article_search_form[q]').send_keys('kaffee', Keys.ENTER)
        driver.implicitly_wait(10)
        # Locate "Das Kaffee Abo monatlich" and click on it:
        driver.find_element_by_link_text('Das Kaffee Abo monatlich').click()
        # Check details of Kaffee Abo:
        assert ('Das Kaffee Abo monatlich' in driver.page_source)
        assert ('16,00 €' in driver.page_source)
        assert ('Menge' in driver.page_source)
        driver.find_element_by_id('line_item_requested_quantity').is_enabled()
        driver.find_element_by_id('buy-button').is_enabled()
        # Choose amount and put in cart:
        #driver.find_element_by_id('line_item_requested_quantity').send_keys('1')
        driver.find_element_by_id('buy-button').click()
        assert ('Der Artikel wurde dem' in driver.page_source)
        # Click on "Warenkorb" in the menu area:
        driver.find_element_by_link_text('Warenkorb (1)').click()
        time.sleep(10)
        # Check details of Kaffee Abo in cart:
        assert ('Dein Warenkorb' in driver.page_source)
        assert ('Das Kaffee Abo monatlich' in driver.page_source)
        assert ('16,00 €' in driver.page_source)
        assert ('Menge' in driver.page_source)
        assert ('Gesamtbetrag' in driver.page_source)
        assert ('Zur Kasse (über den Sicherheitsserver)' in driver.page_source)
        driver.find_element_by_id('line_item_requested_quantity_input').is_enabled()
        driver.find_element_by_id('line_item_requested_quantity_input').is_enabled()
    else:
        print('The cart was not empty; test was not executed.')